

Git figures out that it\u2019s a rename implicitly, so it doesn\u2019t matter if you rename a file that way or with the mv command. The only real difference is that git mv is one command instead of three \u2013 it\u2019s a convenience function. More importantly, you can use any tool you like to rename a file, and address the add/rm later, before you commit.
prev | next
